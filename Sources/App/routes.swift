import Crypto
import Vapor

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    
    router.get("") { req in
        return "Hello World!"
    }
    
    // public routes
    let userController = UserController()
    router.post("api/v1/users", use: userController.create)
    
    // basic / password auth protected routes
    let basic = router.grouped(User.basicAuthMiddleware(using: BCryptDigest()))
    basic.post("api/v1/login", use: userController.login)
    
    // bearer / token auth protected routes
    let bearer = router.grouped(User.tokenAuthMiddleware())
    let todoController = TodoController()
    bearer.get("api/v1/todos", use: todoController.index)
    bearer.post("api/v1/todos", use: todoController.create)
    bearer.delete("api/v1/todos", Todo.parameter, use: todoController.delete)
    
    // Test
}
