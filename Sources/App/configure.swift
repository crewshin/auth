import Authentication
import FluentPostgreSQL
import Vapor

/// Called before your application initializes.
public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
    // Register providers first
    try services.register(FluentPostgreSQLProvider())
    try services.register(AuthenticationProvider())

    // Register routes to the router
    let router = EngineRouter.default()
    try routes(router)
    services.register(router, as: Router.self)

    // Register middleware
    var middlewares = MiddlewareConfig() // Create _empty_ middleware config
//    middlewares.use(SessionsMiddleware.self) // Enables sessions.
//    middlewares.use(FileMiddleware.self) // Serves files from `Public/` directory
    middlewares.use(ErrorMiddleware.self) // Catches errors and converts to HTTP response
    services.register(middlewares)

    // Configure a SQLite database
    let hostname = Environment.get("DATABASE_HOSTNAME") ?? "localhost"
    let port = Int(Environment.get("DATABASE_PORT") ?? "5432") ?? 5432
    let username = Environment.get("DATABASE_USER") ?? "postgres"
    let database = Environment.get("DATABASE_DB")
    let password = Environment.get("DATABASE_PASSWORD")
    let postgres = PostgreSQLDatabase(config: .init(hostname: hostname, port: port, username: username, database: database, password: password, transport: PostgreSQLConnection.TransportConfig.cleartext))

    // Register the configured SQLite database to the database config.
    var databases = DatabasesConfig()
    databases.enableLogging(on: .psql)
    databases.add(database: postgres, as: .psql)
    services.register(databases)

    /// Configure migrations
    var migrations = MigrationConfig()
    migrations.add(model: User.self, database: .psql)
    migrations.add(model: UserToken.self, database: .psql)
    migrations.add(model: Todo.self, database: .psql)
    services.register(migrations)
    
}
